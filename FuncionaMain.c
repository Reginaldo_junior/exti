/**
 * libopencm3/include/libopencm3/cm3/nvic.h:51:
 *			 #define NVIC_ISER(iser_id)    MMIO32(NVIC_BASE + 0x00 + ((iser_id) * 4))
 *
 *
 *
 *
 * libopencm3/include/libopencm3/cm3/common.h:69:
 *					#define MMIO32(addr)		(*(volatile uint32_t *)(addr))
 *
 *
 * libopencm3/include/libopencm3/cm3/nvic.h
 *
 * */

//-------------------- Tipos de dados --------------------  
#define __IO volatile  //Habilita RW
#define uint32_t unsigned int
#define u32 unsigned int

#define uint8_t unsigned char
#define u8 unsigned char

#define int32_t int

#define uint16_t unsigned short
#define u16 unsigned short

#define STACKINT	0x20000000

//--------------------  GPIOx Def --------------------
#define GPIOA ((GPIO_TypeDef * ) 0x40010800) /* GPIOx Addr Base */
#define GPIOB ((GPIO_TypeDef * ) 0x40010c00) 
#define GPIOC ((GPIO_TypeDef * ) 0x40011000) 
#define GPIOD ((GPIO_TypeDef * ) 0x40011400) 
#define GPIOE ((GPIO_TypeDef * ) 0x40011800) 
#define GPIOF ((GPIO_TypeDef * ) 0x40011c00) 
#define GPIOG ((GPIO_TypeDef * ) 0x40012000) 

//-------------------- TIMx Def -------------------- 
#define TIM1 ((TIM_TypeDef * ) 0x40012c00) /* TIMx Addr Base */
#define TIM2 ((TIM_TypeDef * ) 0x40000000) 
#define TIM3 ((TIM_TypeDef * ) 0x40000400) 
#define TIM4 ((TIM_TypeDef * ) 0x40000800) 
#define TIM5 ((TIM_TypeDef * ) 0x40000c00) 
#define TIM6 ((TIM_TypeDef * ) 0x40001000) 
#define TIM7 ((TIM_TypeDef * ) 0x40001400) 
#define TIM8 ((TIM_TypeDef * ) 0x40013400) 
#define TIM9 ((TIM_TypeDef * ) 0x40014C00) 
#define TIM10 ((TIM_TypeDef * ) 0x40015000) 
#define TIM11 ((TIM_TypeDef * ) 0x40015400) 
#define TIM12 ((TIM_TypeDef * ) 0x40001800) 
#define TIM13 ((TIM_TypeDef * ) 0x40001c00) 
#define TIM14 ((TIM_TypeDef * ) 0x40002000) 
#define FLASH (( FLASH_TypeDef * ) 0x40022000)

//-------------------- AFIO 
#define AFIO (( AFIO_TypeDef *) 0x40010000 )

//-------------------- EXTI 
#define EXTI (( EXTI_TypeDef *) 0x40010400 )

//-------------------- RCC Def -------------------- 
#define RCC (( RCC_TypeDef *) 0x40021000) /* RCC Addr Base */

/*-------------------- NVIC --------------------*/
#define NVIC_BASE (( __IO u32 *) 0xE000E100) 
// #define NVIC_ISER		(( (__IO u32 * ) NVIC_BASE + 0x00 ))
// #define NVIC_ICER		(( (__IO u32 * ) NVIC_BASE + 0x80 ))
// #define NVIC_ISPR		(( (__IO u32 * ) NVIC_BASE + 0x100 ))
// #define NVIC_ICPR		(( (__IO u32 * ) NVIC_BASE + 0x180 ))
// #define NVIC_IABR		(( (__IO u32 * ) NVIC_BASE + 0x200 ))
// #define NVIC_IPR		(( (__IO u32 * ) NVIC_BASE + 0x300 ))
#define NVIC            ((NVIC_type  *)  NVIC_BASE)


//-------------------- Area de funções ---------------  

typedef struct {
   uint32_t   ISER[8];     /* Address offset: 0x000 - 0x01C */
   uint32_t  RES0[24];     /* Address offset: 0x020 - 0x07C */
   uint32_t   ICER[8];     /* Address offset: 0x080 - 0x09C */
   uint32_t  RES1[24];     /* Address offset: 0x0A0 - 0x0FC */
   uint32_t   ISPR[8];     /* Address offset: 0x100 - 0x11C */
   uint32_t  RES2[24];     /* Address offset: 0x120 - 0x17C */
   uint32_t   ICPR[8];     /* Address offset: 0x180 - 0x19C */
   uint32_t  RES3[24];     /* Address offset: 0x1A0 - 0x1FC */
   uint32_t   IABR[8];     /* Address offset: 0x200 - 0x21C */
   uint32_t  RES4[56];     /* Address offset: 0x220 - 0x2FC */
   uint8_t   IPR[240];     /* Address offset: 0x300 - 0x3EC */
   uint32_t RES5[644];     /* Address offset: 0x3F0 - 0xEFC */
   uint32_t       STIR;    /* Address offset:         0xF00 */
} NVIC_type;


typedef struct{
	__IO u32 CRL ; 		// 0x00 MODO de operação Port configuration register low
	__IO u32 CRH ; 		// 0x04 Port configuration register high
	__IO u32 IDR ;		// 0x08 Port input data register
	__IO u32 ODR ;		// 0x0c Port output data register
	__IO u32 BSRR ;		// 0x10 Port bit set/reset register
	__IO u32 BRR ;		// 0x14 Port bit reset register
	__IO u32 LCKR ;		// 0x18 Port configuration lock register

} GPIO_TypeDef ;

typedef struct{
		
	__IO u32 EVCR;		// 0x00
	__IO u32 MAPR;		// 0x04
	__IO u16 EXTICR1; 	// 0X08
	__IO u16 RES1 ; 	// 
	__IO u16 EXTICR2; 	// 0x0c
	__IO u16 RES2 ; 	// 
	__IO u16 EXTICR3; 	// 0x10
	__IO u16 RES3 ; 	//
	__IO u16 EXTICR4; 	// 0x14
	__IO u16 RES4 ; 	// 
	__IO u32 reservado;	// 0x18
	__IO u32 MAPR2;		// 0x1c

} AFIO_TypeDef ;

typedef struct{

	__IO u32 CR1; 		// 0x00
	__IO u32 CR2;		// 0x04
	__IO u32 SMCR;		// 0x08
	__IO u32 DIER;		// 0x0c
	__IO u32 SR; 		// 0x10
	__IO u32 EGR; 		// 0x14
	__IO u32 CCMR1;		// 0x18 ->  input/output capture mode
	__IO u32 CCMR2;		// 0x1c ->  input/output capture mode
	__IO u32 CCER; 		// 0x20 <--- 
	__IO u32 CNT; 		// 0x24
	__IO u32 PSC;		// 0x28
	__IO u32 ARR;		// 0x2c
	__IO u32 RCR;		// 0x30
	__IO u32 CCR1; 		// 0x34
	__IO u32 CCR2;		// 0x38 
	__IO u32 CCR3;		// 0x3c
	__IO u32 CCR4;		// 0x40
	__IO u32 BDTR;		// 0x44 -> Descritivo na pagina 344 do RM0008
	__IO u32 DCR;		// 0x48
	__IO u32 DMAR;		// 0x4c

} TIM_TypeDef ;

typedef struct{
//		       		  Offset
	__IO u32 CR; 		// 0x00
	__IO u32 CFGR;		// 0x04 
	__IO u32 CIR;		// 0x08
	__IO u32 APB2RSTR;	// 0x0c
	__IO u32 APB1RSTR;	// 0x10
	__IO u32 AHBENR;	// 0x14
	__IO u32 APB2ENR;	// 0x18
	__IO u32 APB1ENR;	// 0x1c
	__IO u32 BDCR;		// 0x20
	__IO u32 CSR;		// 0x24
	__IO u32 AHBRSTR;	// 0x28
	__IO u32 CFGR2;		// 0x2c

} RCC_TypeDef;

typedef struct{
	
	__IO u32 GOTGCTL; 		// 0x00
   __IO u32 GOTGINT;		// 0x04
   __IO u32 GAHBCFG;		/* 0x08 <<- Responsavel por habilitar 
					  interrupções AHB ou USB */
   __IO u32 GUSBCFG;		// 0x0c
  	__IO u32 GRSTCTL;		// 0x10
  	__IO u32 GINTSTS;		// 0x14
  	__IO u32 GINTMSK;		// 0x18
  	__IO uint16_t GRXSTSR_1_H;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_1_D;	// 0x1c Host/Device Mode
  	__IO uint16_t GRXSTSR_2_H;	// 0x20 Host/Device Mode
  	__IO uint16_t GRXSTSRPR_2_D;	// 0x20 Host/Device Mode
   __IO u32 GRXFSIZ;		// 0x24
	__IO uint16_t  HNPTXFSIZ;	// 0x28 
	__IO uint16_t  DIEPTXF0;	// 0x28
	__IO u32 HNPTXSTS;		// 0x2c

} OTG_FS_TypeDef;

typedef struct {
	
	__IO u32 IMR ;		// 0x00
	__IO u32 EMR ;		// 0x04	
	__IO u32 RTSR ;		// 0x08	
	__IO u32 FTSR ;		// 0x0c	
	__IO u32 SWIER ;	// 0x10	
	__IO u32 PR ;		// 0x14	

} EXTI_TypeDef;

void SystemInit (void) {
  RCC->CR |= (uint32_t)0x00000001;
  RCC->CFGR &= (uint32_t)0xF8FF0000;
  RCC->CR &= (uint32_t)0xFEF6FFFF;
  RCC->CR &= (uint32_t)0xFFFBFFFF;
  RCC->CFGR &= (uint32_t)0xFF80FFFF;
  //RCC->CIR = 0x009F0000;
 }

//--------------------------------------------------
/* Variáveis globais de auxilio */
__IO u8  statusIF =1;
__IO u8  stInterr =1;

//--------------------------------------------------

void Delay ( __IO u32 T ) ;
void mensureTime();

void TOVIVO(){
	for (u8 i=0; i<40; i++){
			  GPIOB->ODR   ^= 1<<9;
			  GPIOC->ODR	^= 1<<13;
			  Delay(75);
	}
EXTI->PR |= 0xffffu;
}

void EXTI1_IRQHandler  (){
	stInterr ^= 1;
	TOVIVO();
	EXTI->PR |= 0b111<<1;	// EXTI1
//	mensureTime();
}

void EXTI3_IRQHandler (){
	TOVIVO();}
void EXTI2_IRQHandler (){
	TOVIVO();}
void EXTI0_IRQHandler (){
	TOVIVO();}
void EXTI9_5_IRQHandler(){
	TOVIVO(); }
void EXTI15_10_IRQHandler(){
	TOVIVO();}


void set_system_clock_to_25Mhz (void){
	RCC->CR 	|= 1<<16 ;
	while (!( RCC->CR & (1<<17) ));	
	RCC->CFGR 	|= 1<<0;
}

void enable_TIM3_delay (void) {
	RCC->APB1ENR |= 1<<1 ;
	TIM3->CR1 = 0x0000;
	TIM3->PSC = 25; // 25-1
	TIM3->ARR = 0XFFFF;
	TIM3->CR1 |= 1<<0;
}

void configEXTI(){
}

void mensureTime(){
   u16 miliseg	 =0;
	statusIF		^=1;

/* Se o status for igual à 0, o contador é reiniciado.*/
	if ( statusIF == 0 )
	  TIM3->EGR |= ( 1 << 0 );

	while ( ((stInterr & ( 1<<1 )) == 0) || miliseg < 3000 ){ 
			TIM3->EGR |= 1<<0 ;
			while ( TIM3->CNT < 590 );
			miliseg++;
		  // criar função de confições antes de atingir o timout
	}

}

void Delay ( __IO u32 T ) {
        for ( T ; T > 0 ; T-- ){
                TIM3->EGR |= ( 1 << 0 );
         while ( TIM3->CNT < 595 ) ;
        }
}


int32_t main (void) {
	set_system_clock_to_25Mhz();
	RCC->APB2ENR  |= 0x7u<<2 ; /*GPIO A B C */


	GPIOC->CRH |= 0x03u <<20;
	GPIOB->CRH |= 0b0011u <<4;

	enable_TIM3_delay(); /* TIM3 */

	GPIOC->ODR	^= (1<<13);		

//	configEXTI(); /* A1 -> EXTI1 */
// Expanção configEXTI -----------------------------------

					 /* Configuração para o pin A1 -> EXTI1*/

						 RCC->APB2ENR  |=1u<<0; /* AFIO */
						 GPIOA->CRH		|= 0x04 <<8; /* Pino 10 → inputFloat*/
						 GPIOA->CRH		|= 0x04 <<12; /* Pino 11 → inputFloat*/

						 GPIOB->CRL		|= 0x04u <<4; /* Pino B1 → inputFloat*/
						 GPIOB->CRL		|= 0x04u <<0; /* Pino B0 → inputFloat*/

						 /*U = unsigned ; UL = Unsigned Long */
						 AFIO->EXTICR1	&= ~0xffffU; /* clean */
						 AFIO->EXTICR1	|= 0x1u<<4; /* tabela_2 referência a GPIOB */
						 AFIO->EXTICR1	|= 0x1u<<0; /* tabela_2 referência a GPIOB */

						 AFIO->EXTICR3	 = 0x0<<8; /* tabela_2 referência a GPIOA -> PA1 */
						 AFIO->EXTICR3	 = 0x0<<12; /* tabela_2 referência a GPIOA -> PA1 */

					 /* Habilita a requisição de interrupção EXTI1 */
						 /*
						 en_nvic(6); // EXTI0_IRQHandler 
						 en_nvic(7); //  EXTI1_IRQHandler 
						 en_nvic(8); //  EXTI2_IRQHandler 
						 en_nvic(9); //  EXTI3_IRQHandler 
						 en_nvic(40); // EXTI15_10_IRQHandler 40
						 en_nvic(23); // EXTI9_5_IRQHandler 23
						*/
						 NVIC->ISER[0] = (1 << 6);
						 NVIC->ISER[0] = (1 << 7);


					 /* Seleção de borda
								  * RISING 	↑  RTSR 1 FTSR 0
								  * FALLING	 ↓ RTSR 0 FTSR 1
								  * BOTH		↑↓ RTSR 1 FTSR 1
								  * */
						 EXTI->RTSR |= 1<<1;
						 EXTI->FTSR &= 0<<1;

					 /* Habilita a mascara de interrupções "IMR" e o evento de
					  * interrupção "EMR"*/
						 EXTI->IMR  |= 1<<1;
						 EXTI->EMR  |= 1<<1;

					 //TO LOCO
						 
						 EXTI->RTSR |= 0xffffu;
						 EXTI->FTSR |= 0xffffu;
						 EXTI->IMR  |= 0xffffu;
						 EXTI->EMR  |= 0xffffu;
						 GPIOB->ODR  ^= 1<<9;
// FIM Expanção configEXTI -------------------------------

	GPIOC->ODR	|= (1<<13);		
 while (1){
		
	Delay(500);
	GPIOC->ODR	^= (1<<13);		

	//if (GPIOA->IDR & (1<<1) && 1)
	if ((EXTI->PR & (1<<0)) == 1){ // Leitura do flag de interrupção
		for (u8 i=0; i<=2; i++){
			Delay(50);
		   GPIOB->ODR  ^= 1<<9;
			EXTI->PR = 0xffffu ;
		}
	}
 }

	return 0;
	/*		Este codigo não chama a requisição de interrupção
	 * por motivos desconhecidos para mim. 
	 *		Estou profundamente frustado
	 * com isto. Há um teste que verifica a flag do
	 * registrador EXTI e está funcionando.
	 * */
}

/* Definições de bits "CFG" e "Mode" da configuração das GPIOs.
 *		   Pin_Hardw	
 *   CFG     MODE    H	 L   
 * |31|30|  |29|28| 15 ; 7		
 * |27|26|  |25|24| 14 ; 6	
 * |23|22|  |21|20| 13 ; 5
 * |19|18|  |17|16| 12 ; 4
 * |15|14|  |13|12| 11 ; 3
 * |11|10|  | 9| 8| 10 ; 2
 * | 7| 6|  | 5| 4|  9 ; 1
 * | 3| 2|  | 1| 0|  8 ; 0
 *
 *
 * __________________________________________
 * HexaCode -> 50MHz
 * 0x03 | 0| 0|  |01..11| <- Out Push-pull
 * 0x07 | 0| 1|  |01..11| <- Out Open-drain
 * 0x0B | 1| 0|  |01..11| <- Alt Out *pull
 * 0x0B | 1| 0|  |01..11| <- Alt Out *drain
 * __________________________________________
 * 0x00 | 0| 0|  |  00  | -< Input Analogig
 * 0x04 | 0| 1|  |  00  | -< Input Floating  
 * 0x08 | 1| 0|  |  00  | -< Input Pull-{down,up}
 * __________________________________________
 * */

/* tabela_2 -> Configuração do Registrador AFIO->EXTICRx
 *     EXTI[0..15] 3    2    1    0 
 *
 *     Shift     <<12  <<8  <<4  <<0 	
 *	              xxxx xxxx xxxx xxxx 
 * GPIO[a..g] pin  3    2    1    0   	AFIO_EXTI_CR_1
 * GPIO[a..g] pin  7    6    5    4  	AFIO_EXTI_CR_2 
 * GPIO[a..g] pin  11   10   9    8  	AFIO_EXTI_CR_3 
 * GPIO[a..g] pin  15   14   13   12  	AFIO_EXTI_CR_4 
 *
 *Hex  xxxx
 *0x00 0000: PA[x] pin 
 *0x01 0001: PB[x] pin 
 *0x02 0010: PC[x] pin 
 *0x03 0011: PD[x] pin 
 *0x04 0100: PE[x] pin 
 *0x05 0101: PF[x] pin 
 *0x06 0110: PG[x] pin
 *
 * */

/* link_1 https://developer.arm.com/documentation/dui0662/b/Cortex-M0--Peripherals/Nested-Vectored-Interrupt-Controller/Interrupt-Set-Enable-Register
 *
 *
 *
 * https://github.com/98zam98/arm_nvic_driver/blob/main/hardware_arm_nvic_driver.h
 * **/
